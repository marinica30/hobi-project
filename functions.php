<?php 

function testInput($input) {

  	$input = trim($input);
  	$input = stripslashes($input);
  	$input = htmlspecialchars($input);
  	return $input;
}

function unsetUserInfo() {
	unset($_SESSION['id']);
	unset($_SESSION['username']);
	unset($_SESSION['password']);
	unset($_SESSION['firstname']);
	unset($_SESSION['lastname']);
	unset($_SESSION['address']);
	unset($_SESSION['email']);
	unset($_SESSION['age']);
}	 

 ?>