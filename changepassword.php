<?php 

include 'core.php';
include 'functions.php';
include 'left-side.php';

if(isset($_SESSION['id'])) {
	$userID = $_SESSION['id'];	
	$query = mysqli_query($conn,"SELECT * FROM users WHERE id='".$userID."'");
	while ($rows = mysqli_fetch_array($query)) {
   		$_SESSION['password'] = $rows['password'];
	}
	//unset($_SESSION['id']);
}else {
	header("Location: login.php");
	die("Nelogat");
}
 ?>
<div id="profile">
	<img src="images/defaultProfilePicture.png" alt="Atentie!" align="left" id="alergii_curente" width="500" height="500" >

	 <h2>Schimbare parola</h2>

		<form method="post">
			Parola Veche: <input type="password" name="oldPassword" value="" placeholder="Parola curenta"><br>
			Parola Noua: <input type="password" name="newPassword" value="" placeholder="Parola noua"><br>
			Reintroduceti Noua Parola: <input type="password" name="confirmPassword" value="" placeholder="Confirmare Parola"><br>

			<?php
				if(isset($_POST['oldPassword']) && isset($_POST['newPassword']) && isset($_POST['confirmPassword'])) {

					$oldPassword = mysqli_real_escape_string($conn, $_POST['oldPassword']);
					$newPassword = mysqli_real_escape_string($conn, $_POST['newPassword']);
					$confirmedPassword = mysqli_real_escape_string($conn, $_POST['confirmPassword']);
					$hashedPassword = md5($confirmedPassword);

					if (md5($oldPassword) <> $_SESSION['password']) {
						?> <font color="red">Parola curenta nu corespunde cu cea introdusa.</font> <?php
					}else if ($newPassword <> $confirmedPassword) { 
						?> <font color="red">Parolele nu corespund.</font> <?php
					} else if (strcmp($oldPassword, $newPassword) == 0){
						?> <font color="red">Noua parola introdusa coincide cu parola curenta.</font> <?php
					} else if (mysqli_query($conn, "UPDATE users SET password='$hashedPassword' WHERE id='$userID'")) {
						?> <font color="green">Ati schimbat parola cu succes.</font> <?php
						header("Refresh: 2; url=profile.php");
					} else { 
						mysqli_error($conn); 
					}
				}
			?>

			<input type="submit" class="saveButton" value="Schimba parola">
		</form>

		<!-- <center><button class="saveButton" style="vertical-align:middle"><span>Save </span></button></center> -->

</div>
<br>
<br>
</body>

<?php
	include 'footer.php'
?>

</html>