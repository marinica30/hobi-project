-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 06 Iun 2016 la 21:02
-- Versiune server: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `Hobi`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `allen`
--

CREATE TABLE IF NOT EXISTS `allen` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `simptoms` text,
  `treatment` text,
  `city` varchar(255) DEFAULT NULL,
  `season` varchar(255) NOT NULL,
  `visibility` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `allen`
--

INSERT INTO `allen` (`id`, `name`, `simptoms`, `treatment`, `city`, `season`, `visibility`, `time`) VALUES
(1, 'Alergia la gluten', 'tuse', 'ceaiuri', 'Munchen', 'toamna', 'public', '2016-06-04 20:34:48');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `phobias`
--

CREATE TABLE IF NOT EXISTS `phobias` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `simptoms` text,
  `treatment` text,
  `city` varchar(255) DEFAULT NULL,
  `season` varchar(255) DEFAULT NULL,
  `visibility` varchar(255) NOT NULL,
  `user` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `phobias`
--

INSERT INTO `phobias` (`id`, `name`, `simptoms`, `treatment`, `city`, `season`, `visibility`, `user`, `time`) VALUES
(1, 'Alergia la praf', 'tuse', 'antibiotice', 'Tulcea', 'Primavara', 'privat', 'JAMES61', '2016-06-04 14:14:17'),
(2, 'Alergia la polen', 'tuse,secretii nazale', 'ceaiuri', 'Mangalia', 'Primavara', 'public', 'JAMES61', '2016-06-04 14:14:15'),
(3, 'Alergia la pisici', 'tuse', 'ceaiuri', 'Ploiesti', 'Toamna', 'public', 'JAMES61', '2016-06-04 14:14:11'),
(4, 'Alergia la furnici', 'tuse', 'antibiotice', 'Constanta', 'primavara', 'public', 'JAMES61', '2016-06-04 14:04:52'),
(5, 'Alergia la gluten', 'tuse', 'ceaiuri', 'Munchen', 'toamna', 'public', 'Allen', '2016-06-04 20:34:48');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `james60`
--

CREATE TABLE IF NOT EXISTS `james60` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `simptoms` text,
  `treatment` text,
  `city` varchar(255) DEFAULT NULL,
  `season` varchar(255) DEFAULT NULL,
  `visibility` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `james61`
--

CREATE TABLE IF NOT EXISTS `james61` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `simptoms` text,
  `treatment` text,
  `city` varchar(255) DEFAULT NULL,
  `season` varchar(255) DEFAULT NULL,
  `visibility` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `james61`
--

INSERT INTO `james61` (`id`, `name`, `simptoms`, `treatment`, `city`, `season`, `visibility`, `time`) VALUES
(1, 'Alergia la praf', 'tuse', 'ceaiuri', 'Mures', 'primavara', 'public', '2016-06-06 17:57:52'),
(15, 'Alergia la polen', 'tuse,secretii nazale', '', 'Dublin', 'primavara', 'public', '2016-06-04 13:33:29'),
(18, 'Alergia la furnici', 'tuse', 'antibiotice', 'Ilfov', 'toamna', 'public', '2016-06-06 17:43:24');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `medical_advices`
--

CREATE TABLE IF NOT EXISTS `medical_advices` (
  `p_phobias` text NOT NULL,
  `f_phobias` text NOT NULL,
  `p_treatment` text NOT NULL,
  `f_treatment` text NOT NULL,
`id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `medical_advices`
--

INSERT INTO `medical_advices` (`p_phobias`, `f_phobias`, `Present_treatment`, `Future_treatment`, `id`) VALUES
('In aceasta perioada, alergia la polen este cea mai raspandita.Va rugam sa consultati cu atentie sfaturile urmatoare:', 'In urmatoare, alergia la praf este cea mai raspandita.Va rugam sa consultati cu atentie sfaturile urmatoare:', 'Cand simptomele sunt accentuate si iti provoaca disconfort, ar fi bine sa te adresezi medicului, care iti poate recomanda tratamentul adecvat. Medicamentele simptomatice contracta vasele sanguine din zonele inflamate (nas, ochi) si  diminueaza congestia, secretiile, stranuturile. Ele se administreaza local, sub forma de picaturi pentru nas sau ochi. Important este sa le folosesti doar cateva zile, pentru ca utilizarea lor poate conduce la aparitia rinitei cronice.', 'Cum majoritatea populatiei afectate de acest tip de alergii se incadreaza in categoriile de varsta copii si tineri, sunt importante atat diagnosticul si tratamentul precoce, cat si educatia alergologica privind preventia dezvoltarii altor complicatii alergice suplimentare. Avand in vedere ca factorii dovediti contributori ai cresterii frecventei, severitatii si heterogenitatii afectiunilor alergice actuale sunt inca evolutivi, numind astfel urbanizarea, industrializarea, cresterea poluarii, schimbarile climatice si nutritionale etc, singura optiune ramane optimizarea abordarii strategiilor preventive si de tratament individualizate fiecarui pacient de catre medicul sau alergolog.', 1);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `news_feed`
--

CREATE TABLE IF NOT EXISTS `news_feed` (
`id` int(6) NOT NULL,
  `user` varchar(255) NOT NULL,
  `id_story` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `news_feed`
--

INSERT INTO `news_feed` (`id`, `user`, `id_story`, `time`) VALUES
(4, 'JAMES61', 12, '2016-05-31 07:04:37'),
(5, 'JAMES61', 14, '2016-05-31 07:53:48'),
(6, 'JAMES61', 15, '2016-06-04 13:33:29'),
(7, 'JAMES61', 16, '2016-06-04 13:48:27'),
(8, 'JAMES61', 17, '2016-06-04 13:51:03'),
(9, 'JAMES61', 18, '2016-06-04 13:51:57'),
(10, 'Allen', 1, '2016-06-04 20:34:48');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `address`, `age`, `firstname`, `lastname`, `token`) VALUES
(1, 'test', '098f6bcd4621d373cade4e832627b4f6', 'test@test.com', 'Strada XF', 11, 'Ema', 'Lola', ''),
(2, 'JAMES50', '098f6bcd4621d373cade4e832627b4f6', 'james50@james50.com', '', 0, '', '', ''),
(3, 'JAMES52', '5061cb7b5448f3bd8559581e4deec07d', 'JAMES52@James52.com', '', 0, '', '', ''),
(4, 'JAMES53', '4832526732fdf34b416ed9ea74358ece', 'JAMES53@yahoo.com', '', 0, '', '', ''),
(5, 'JAMES54', 'dc16809c4ba96881510e953b9e28bd1d', 'JAMES54@James54.com', '', 0, '', '', ''),
(6, 'JAMES58', 'bef7d8f1e5a2dc90ac90137203c5df78', 'JAMES58@JAMES58.com', '', 0, '', '', ''),
(7, 'JAMES60', '571f4579579ceeea03355e39b74f62b8', 'JAMES60@JAMES60.com', '', 0, '', '', ''),
(8, 'JAMES60', '571f4579579ceeea03355e39b74f62b8', 'JAMES60@JAMES60.com', '', 0, '', '', ''),
(9, 'JAMES61', '4ccdd30a10d31f74664a043f86da42ae', 'JAMES61@JAMES61.com', '', 0, '', '', ''),
(10, 'Allen', '9c0ca0cabbd78a5a02bf8447347eced5', 'Allen@Allen.com', '', 0, '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allen`
--
ALTER TABLE `allen`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phobias`
--
ALTER TABLE `phobias`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`), ADD FULLTEXT KEY `name` (`name`);

--
-- Indexes for table `james60`
--
ALTER TABLE `james60`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `james61`
--
ALTER TABLE `james61`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medical_advices`
--
ALTER TABLE `medical_advices`
 ADD PRIMARY KEY (`id`), ADD FULLTEXT KEY `p_phobias` (`p_phobias`);

--
-- Indexes for table `news_feed`
--
ALTER TABLE `news_feed`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `allen`
--
ALTER TABLE `allen`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `phobias`
--
ALTER TABLE `phobias`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `james60`
--
ALTER TABLE `james60`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `james61`
--
ALTER TABLE `james61`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `medical_advices`
--
ALTER TABLE `medical_advices`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `news_feed`
--
ALTER TABLE `news_feed`
MODIFY `id` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
